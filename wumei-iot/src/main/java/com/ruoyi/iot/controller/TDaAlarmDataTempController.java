package com.ruoyi.iot.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.iot.domain.TDaAlarmDataTemp;
import com.ruoyi.iot.service.ITDaAlarmDataTempService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报警数据表Controller
 * 
 * @author lyd
 * @date 2023-11-30
 */
@Api("DMS报警数据")
@RestController
@RequestMapping("/iot/alarm")
public class TDaAlarmDataTempController extends BaseController
{
    @Autowired
    private ITDaAlarmDataTempService tDaAlarmDataTempService;

    /**
     * 查询报警数据表列表
     */
    @PreAuthorize("@ss.hasPermi('iot:alarm:list')")
    @GetMapping("/list")
    @ApiOperation("查询报警数据表列表")
    public TableDataInfo list(TDaAlarmDataTemp tDaAlarmDataTemp)
    {
        startPage();
        List<TDaAlarmDataTemp> list = tDaAlarmDataTempService.selectTDaAlarmDataTempList(tDaAlarmDataTemp);
        return getDataTable(list);
    }

    /**
     * 导出报警数据表列表
     */
    @PreAuthorize("@ss.hasPermi('iot:alarm:export')")
    @Log(title = "报警数据表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TDaAlarmDataTemp tDaAlarmDataTemp)
    {
        List<TDaAlarmDataTemp> list = tDaAlarmDataTempService.selectTDaAlarmDataTempList(tDaAlarmDataTemp);
        ExcelUtil<TDaAlarmDataTemp> util = new ExcelUtil<TDaAlarmDataTemp>(TDaAlarmDataTemp.class);
        util.exportExcel(response, list, "报警数据表数据");
    }

    /**
     * 获取报警数据表详细信息
     */
    @PreAuthorize("@ss.hasPermi('iot:alarm:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("报警详情")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tDaAlarmDataTempService.selectTDaAlarmDataTempById(id));
    }

    /**
     * 新增报警数据表
     */
    @PreAuthorize("@ss.hasPermi('iot:alarm:add')")
    @Log(title = "报警数据表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TDaAlarmDataTemp tDaAlarmDataTemp)
    {
        return toAjax(tDaAlarmDataTempService.insertTDaAlarmDataTemp(tDaAlarmDataTemp));
    }

    /**
     * 修改报警数据表
     */
    @PreAuthorize("@ss.hasPermi('iot:alarm:edit')")
    @Log(title = "报警数据表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TDaAlarmDataTemp tDaAlarmDataTemp)
    {
        return toAjax(tDaAlarmDataTempService.updateTDaAlarmDataTemp(tDaAlarmDataTemp));
    }

    /**
     * 删除报警数据表
     */
    @PreAuthorize("@ss.hasPermi('iot:alarm:remove')")
    @Log(title = "报警数据表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tDaAlarmDataTempService.deleteTDaAlarmDataTempByIds(ids));
    }
}