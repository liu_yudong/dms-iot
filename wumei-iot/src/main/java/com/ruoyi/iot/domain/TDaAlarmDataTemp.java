package com.ruoyi.iot.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 报警数据表对象 t_da_alarm_data_temp
 * 
 * @author lyd
 * @date 2023-11-30
 */
public class TDaAlarmDataTemp {
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 设备编码 */
    @Excel(name = "设备编码")
    private String equipcode;

    /** 报警编码 */
    @Excel(name = "报警编码")
    private String alarmcode;

    /** 报警信息 */
    @Excel(name = "报警信息")
    private String alarmtext;

    /** 报警时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "报警时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date alarmtime;

    /** 清除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "清除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cleartime;

    /** 是否活跃{0否，1是} */
    @Excel(name = "是否活跃{0否，1是}")
    private Long active;

    /** 处理状态{0是未处理，98，99已处理} */
    @Excel(name = "处理状态{0是未处理，98，99已处理}")
    private Long datastatus;

    /** 最后存活时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后存活时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastarchivetime;

    /** 工厂编码 */
    @Excel(name = "工厂编码")
    private String plantno;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long dataflag;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEquipcode(String equipcode) 
    {
        this.equipcode = equipcode;
    }

    public String getEquipcode() 
    {
        return equipcode;
    }
    public void setAlarmcode(String alarmcode) 
    {
        this.alarmcode = alarmcode;
    }

    public String getAlarmcode() 
    {
        return alarmcode;
    }
    public void setAlarmtext(String alarmtext) 
    {
        this.alarmtext = alarmtext;
    }

    public String getAlarmtext() 
    {
        return alarmtext;
    }
    public void setAlarmtime(Date alarmtime) 
    {
        this.alarmtime = alarmtime;
    }

    public Date getAlarmtime() 
    {
        return alarmtime;
    }
    public void setCleartime(Date cleartime) 
    {
        this.cleartime = cleartime;
    }

    public Date getCleartime() 
    {
        return cleartime;
    }
    public void setActive(Long active) 
    {
        this.active = active;
    }

    public Long getActive() 
    {
        return active;
    }
    public void setDatastatus(Long datastatus) 
    {
        this.datastatus = datastatus;
    }

    public Long getDatastatus() 
    {
        return datastatus;
    }
    public void setLastarchivetime(Date lastarchivetime) 
    {
        this.lastarchivetime = lastarchivetime;
    }

    public Date getLastarchivetime() 
    {
        return lastarchivetime;
    }
    public void setPlantno(String plantno) 
    {
        this.plantno = plantno;
    }

    public String getPlantno() 
    {
        return plantno;
    }
    public void setDataflag(Long dataflag) 
    {
        this.dataflag = dataflag;
    }

    public Long getDataflag() 
    {
        return dataflag;
    }

    public Date getUpdateTime()
    {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("equipcode", getEquipcode())
            .append("alarmcode", getAlarmcode())
            .append("alarmtext", getAlarmtext())
            .append("alarmtime", getAlarmtime())
            .append("cleartime", getCleartime())
            .append("active", getActive())
            .append("datastatus", getDatastatus())
            .append("lastarchivetime", getLastarchivetime())
            .append("updateTime", getUpdateTime())
            .append("plantno", getPlantno())
            .append("dataflag", getDataflag())
            .toString();
    }
}