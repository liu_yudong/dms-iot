package com.ruoyi.iot.mapper;

import java.util.List;
import com.ruoyi.iot.domain.TDaAlarmDataTemp;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 报警数据表Mapper接口
 * 
 * @author lyd
 * @date 2023-11-30
 */
@Mapper
public interface TDaAlarmDataTempMapper 
{
    /**
     * 查询报警数据表
     * 
     * @param id 报警数据表主键
     * @return 报警数据表
     */
    public TDaAlarmDataTemp selectTDaAlarmDataTempById(Long id);

    /**
     * 查询报警数据表列表
     * 
     * @param tDaAlarmDataTemp 报警数据表
     * @return 报警数据表集合
     */
    public List<TDaAlarmDataTemp> selectTDaAlarmDataTempList(TDaAlarmDataTemp tDaAlarmDataTemp);

    /**
     * 新增报警数据表
     * 
     * @param tDaAlarmDataTemp 报警数据表
     * @return 结果
     */
    public int insertTDaAlarmDataTemp(TDaAlarmDataTemp tDaAlarmDataTemp);

    /**
     * 修改报警数据表
     * 
     * @param tDaAlarmDataTemp 报警数据表
     * @return 结果
     */
    public int updateTDaAlarmDataTemp(TDaAlarmDataTemp tDaAlarmDataTemp);

    /**
     * 删除报警数据表
     * 
     * @param id 报警数据表主键
     * @return 结果
     */
    public int deleteTDaAlarmDataTempById(Long id);

    /**
     * 批量删除报警数据表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTDaAlarmDataTempByIds(Long[] ids);
}