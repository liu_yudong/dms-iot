package com.ruoyi.iot.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.TDaAlarmDataTempMapper;
import com.ruoyi.iot.domain.TDaAlarmDataTemp;
import com.ruoyi.iot.service.ITDaAlarmDataTempService;

/**
 * 报警数据表Service业务层处理
 * 
 * @author lyd
 * @date 2023-11-30
 */
@Service
public class TDaAlarmDataTempServiceImpl implements ITDaAlarmDataTempService 
{
    @Autowired
    private TDaAlarmDataTempMapper tDaAlarmDataTempMapper;

    /**
     * 查询报警数据表
     * 
     * @param id 报警数据表主键
     * @return 报警数据表
     */
    @Override
    public TDaAlarmDataTemp selectTDaAlarmDataTempById(Long id)
    {
        return tDaAlarmDataTempMapper.selectTDaAlarmDataTempById(id);
    }

    /**
     * 查询报警数据表列表
     * 
     * @param tDaAlarmDataTemp 报警数据表
     * @return 报警数据表
     */
    @Override
    public List<TDaAlarmDataTemp> selectTDaAlarmDataTempList(TDaAlarmDataTemp tDaAlarmDataTemp)
    {
        return tDaAlarmDataTempMapper.selectTDaAlarmDataTempList(tDaAlarmDataTemp);
    }

    /**
     * 新增报警数据表
     * 
     * @param tDaAlarmDataTemp 报警数据表
     * @return 结果
     */
    @Override
    public int insertTDaAlarmDataTemp(TDaAlarmDataTemp tDaAlarmDataTemp)
    {
        return tDaAlarmDataTempMapper.insertTDaAlarmDataTemp(tDaAlarmDataTemp);
    }

    /**
     * 修改报警数据表
     * 
     * @param tDaAlarmDataTemp 报警数据表
     * @return 结果
     */
    @Override
    public int updateTDaAlarmDataTemp(TDaAlarmDataTemp tDaAlarmDataTemp)
    {
        return tDaAlarmDataTempMapper.updateTDaAlarmDataTemp(tDaAlarmDataTemp);
    }

    /**
     * 批量删除报警数据表
     * 
     * @param ids 需要删除的报警数据表主键
     * @return 结果
     */
    @Override
    public int deleteTDaAlarmDataTempByIds(Long[] ids)
    {
        return tDaAlarmDataTempMapper.deleteTDaAlarmDataTempByIds(ids);
    }

    /**
     * 删除报警数据表信息
     * 
     * @param id 报警数据表主键
     * @return 结果
     */
    @Override
    public int deleteTDaAlarmDataTempById(Long id)
    {
        return tDaAlarmDataTempMapper.deleteTDaAlarmDataTempById(id);
    }
}