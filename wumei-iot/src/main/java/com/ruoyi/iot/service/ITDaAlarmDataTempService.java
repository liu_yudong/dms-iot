package com.ruoyi.iot.service;

import java.util.List;
import com.ruoyi.iot.domain.TDaAlarmDataTemp;

/**
 * 报警数据表Service接口
 * 
 * @author lyd
 * @date 2023-11-30
 */
public interface ITDaAlarmDataTempService 
{
    /**
     * 查询报警数据表
     * 
     * @param id 报警数据表主键
     * @return 报警数据表
     */
    public TDaAlarmDataTemp selectTDaAlarmDataTempById(Long id);

    /**
     * 查询报警数据表列表
     * 
     * @param tDaAlarmDataTemp 报警数据表
     * @return 报警数据表集合
     */
    public List<TDaAlarmDataTemp> selectTDaAlarmDataTempList(TDaAlarmDataTemp tDaAlarmDataTemp);

    /**
     * 新增报警数据表
     * 
     * @param tDaAlarmDataTemp 报警数据表
     * @return 结果
     */
    public int insertTDaAlarmDataTemp(TDaAlarmDataTemp tDaAlarmDataTemp);

    /**
     * 修改报警数据表
     * 
     * @param tDaAlarmDataTemp 报警数据表
     * @return 结果
     */
    public int updateTDaAlarmDataTemp(TDaAlarmDataTemp tDaAlarmDataTemp);

    /**
     * 批量删除报警数据表
     * 
     * @param ids 需要删除的报警数据表主键集合
     * @return 结果
     */
    public int deleteTDaAlarmDataTempByIds(Long[] ids);

    /**
     * 删除报警数据表信息
     * 
     * @param id 报警数据表主键
     * @return 结果
     */
    public int deleteTDaAlarmDataTempById(Long id);
}